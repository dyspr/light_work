var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize

var numOfCircles = 10
var limit = 0.8
var maxSize = 0.1
var positions = []
var sizes = []
var resolution = 128

var numOfSingleCircles = 21
var singleSize = []
var singlePos = []
var singleColor = []
var levels = []
var singleLimit = 0.7
var maxSingleSize = 0.3

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)

  for (var i = 0; i < numOfCircles; i++) {
    sizes.push([0.1 * maxSize + 0.9 * maxSize * Math.random(), 0.1 * maxSize + 0.9 * maxSize * Math.random()])
    positions.push([
      [-limit * 0.5 + limit * Math.random(), -limit * 0.5 + limit * Math.random()],
      [-limit * 0.5 + limit * Math.random(), -limit * 0.5 + limit * Math.random()]
    ])
  }

  for (var i = 0; i < numOfSingleCircles; i++) {
    singleSize.push(0.1 * maxSingleSize + 0.9 * maxSingleSize * Math.random())
    singlePos.push([-singleLimit * 0.5 + singleLimit * Math.random(), -singleLimit * 0.5 + singleLimit * Math.random()])
    singleColor.push(Math.floor(Math.random() * 256))
    levels.push(32 + Math.floor(Math.random() * 32))
  }
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < singleSize.length; i++) {
    for (var j = 0; j < levels[i]; j++) {
      push()
      translate(windowWidth * 0.5 + singlePos[i][0] * boardSize, windowHeight * 0.5 + singlePos[i][1] * boardSize)
      noStroke()
      fill(singleColor[i] * (1 - j / levels[i]))
      ellipse(0, 0, singleSize[i] * boardSize * (1 - j / levels[i]))
      pop()
    }
  }

  for (var i = 0; i < sizes.length; i++) {
    for (var j = 0; j < resolution; j++) {
      push()
      translate(windowWidth * 0.5 + positions[i][0][0] * boardSize + ((positions[i][1][0] - positions[i][0][0]) * boardSize) * (j / resolution), windowHeight * 0.5 + positions[i][0][1] * boardSize + ((positions[i][1][1] - positions[i][0][1]) * boardSize) * (j / resolution))
      noStroke()
      fill(255 * (1 - j / resolution))
      ellipse(0, 0, sizes[i][0] * boardSize + (sizes[i][1] - sizes[i][0]) * boardSize * (j / resolution))
      pop()
    }
  }

  for (var i = 0; i < singlePos.length; i++) {
    singlePos[i][0] += -0.01 + 0.02 * Math.random()
    singlePos[i][1] += -0.01 + 0.02 * Math.random()
  }

  for (var i = 0; i < positions.length; i++) {
    positions[i][0][0] += -0.01 + 0.02 * Math.random()
    positions[i][0][1] += -0.01 + 0.02 * Math.random()
    positions[i][1][0] += -0.01 + 0.02 * Math.random()
    positions[i][1][1] += -0.01 + 0.02 * Math.random()
  }

  if (frameCount % 2 === 0) {
    sizes = shuffle(sizes)
    singleSize = shuffle(singleSize)
  }

  if (frameCount % 6 === 0) {
    singleSize.push(0.1 * maxSingleSize + 0.9 * maxSingleSize * Math.random())
    singlePos.push([-singleLimit * 0.5 + singleLimit * Math.random(), -singleLimit * 0.5 + singleLimit * Math.random()])
    singleColor.push(Math.floor(Math.random() * 256))

    if (singlePos.length > numOfSingleCircles) {
      singleSize.splice(0, 1)
      singlePos.splice(0, 1)
      singleColor.splice(0, 1)
    }

    sizes.push([0.1 * maxSize + 0.9 * maxSize * Math.random(), 0.1 * maxSize + 0.9 * maxSize * Math.random()])
    positions.push([
      [-limit * 0.5 + limit * Math.random(), -limit * 0.5 + limit * Math.random()],
      [-limit * 0.5 + limit * Math.random(), -limit * 0.5 + limit * Math.random()]
    ])

    if (positions.length > numOfCircles) {
      sizes.splice(0, 1)
      positions.splice(0, 1)
    }
  }
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function create2DArray(numRows, numCols, init, bool) {
  var array = [];
  for (var i = 0; i < numRows; i++) {
    var columns = []
    for (var j = 0; j < numCols; j++) {
      if (bool === true) {
        columns[j] = init
      } else {
        columns[j] = j * numRows + i
      }
    }
    array[i] = columns
  }
  return array
}
